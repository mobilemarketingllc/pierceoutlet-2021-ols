<?php
/**
 * Astra Child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra Child
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ASTRA_CHILD_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'astra-child-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ASTRA_CHILD_VERSION, 'all' );
	wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
  wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
	wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
	wp_enqueue_style( 'bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );
    wp_enqueue_script( 'bootstrap-js', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), true);
}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );



remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
	if ( isset( $atts['facet'] ) ) {       
		$output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
	}
	return $output;
  }, 10, 2 );
  
add_filter( 'facetwp_query_args', function( $query_args, $class ) {
      if( @$query_args['is_featured_temp'] == '1'){
        $query_args['posts_per_page']='4';
        $query_args['orderby'] = 'date';

      }else{
        $query_args['posts_per_page']='12';
		$query_args['meta_key']='collection';
        if(($query_args['post_type'] !="fl-builder-template" && $query_args['post_type'] !="post" && $query_args['post_type'] !="product" && $query_args['post_type'] !="astra-advanced-hook"  && $query_args['post_type'] !="slick-slider" && $query_args['post_type'] != "acf-field") &&  @wp_count_posts($query_args['post_type'])->publish > 1 ){
            add_filter('posts_groupby', 'query_group_by_filter');
        }
		// if( ( isset($query_args['post_type']) && !in_array( $query_args['post_type'] , array('post', 'page', 'attachment', 'revision', 'nav_menu_item', 'custom_css', 'customize_changeset', 'oembed_cache', 'user_request', 'wp_block', 'acf-field-group', 'acf-field', 'product', 'product_variation', 'shop_order', 'shop_order_refund', 'shop_coupon', 'fl-builder-template', 'store-locations', 'fl-theme-layout', 'astra-advanced-hook'))) &&  @wp_count_posts($query_args['post_type'])->publish > 1){
		// 	add_filter('posts_groupby', 'query_group_by_filter');
		// }
      }  
        
    return $query_args;
}, 10, 2 );


function year_shortcode() {
  $year = date('Y');
  return $year;
}
add_shortcode('year', 'year_shortcode');
//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
   
}
add_filter( 'auto_update_plugin', '__return_false' );

//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail_new' );

function wpse_100012_override_yoast_breadcrumb_trail_new( $links ) {
    global $post;

    if ( is_singular( 'hardwood_catalog' )  ) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/hardwood/',
            'text' => 'Hardwood',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/hardwood/browse/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );

    }elseif (is_singular( 'carpeting' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/carpet/',
            'text' => 'Carpet',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/carpet/browse/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );

    }elseif (is_singular( 'luxury_vinyl_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/vinyl/',
            'text' => 'Vinyl',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/vinyl/browse/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }elseif (is_singular( 'laminate_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/laminate/',
            'text' => 'Laminate',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/laminate/browse/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }elseif (is_singular( 'tile_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/tile/',
            'text' => 'Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/tile/browse/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }elseif (is_singular( 'solid_wpc_waterproof' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/waterproof/',
            'text' => 'Waterproof Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/waterproof/browse/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
    }

    return $links;
}